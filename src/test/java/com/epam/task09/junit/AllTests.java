package com.epam.task09.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = {GroupTest.class, StudentTest.class})
public class AllTests {
}
