package com.epam.task09.junit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class StudentTest {

  private static final Logger LOGGER = LogManager.getLogger(StudentTest.class);
  private static final String BAD_SCORE_MESSAGE = "Unexpected score";
  @Rule
  public TestWatcher testWatcher = new TestWatcher() {
    protected void failed(Throwable e, Description description) {
      LOGGER.error("{} failed {}", description.getDisplayName(), e.getMessage());
      super.failed(e, description);
    }
  };

  @Test
  public void testCheckInputScore() {
    Class<Student> aClass = Student.class;
    try {
      Method checkInputScore = aClass.getDeclaredMethod("checkInputScore", double.class);
      checkInputScore.setAccessible(true);
      assertEquals(BAD_SCORE_MESSAGE, 20.0, checkInputScore.invoke(new Student(), 20));
      assertEquals(BAD_SCORE_MESSAGE, 0.0, checkInputScore.invoke(new Student(), -20));
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }
}
