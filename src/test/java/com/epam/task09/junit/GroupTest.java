package com.epam.task09.junit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class GroupTest {

  private static final Logger LOGGER = LogManager.getLogger(GroupTest.class);
  private static Group group;
  @Rule
  public TestWatcher testWatcher = new TestWatcher() {
    protected void failed(Throwable e, Description description) {
      LOGGER.error("{} failed {}", description.getDisplayName(), e.getMessage());
      super.failed(e, description);
    }
  };

  @Before
  public void initializeStudentList() {
    ArrayList<Student> startStudentList = new ArrayList<>();
    startStudentList.add(new Student("Amelia", 72));
    startStudentList.add(new Student("Codi", 89));
    startStudentList.add(new Student("Orest", 65));
    group = new Group("ANUS-12", 3, startStudentList);
  }

  @Test
  public void testSortStudents() {
    ArrayList<Student> sortedList = new ArrayList<>();
    sortedList.add(new Student("Orest", 65));
    sortedList.add(new Student("Amelia", 72));
    sortedList.add(new Student("Codi", 89));
    assertArrayEquals(sortedList, group.sortStudents());
  }

  @Test
  public void testShowStudentList() {
    assertNotNull(group.getStudentsList());
  }

  @Test
  public void testIsListEmpty() {
    Class<Group> aClass = Group.class;
    try {
      Method isListStudentEmpty = aClass.getDeclaredMethod("isStudentListEmpty");
      isListStudentEmpty.setAccessible(true);
      assertTrue((Boolean) isListStudentEmpty.invoke(group));
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testAddStudentInfo() {
    Class<Group> aClass = Group.class;
    try {
      Method addStudentInfo = aClass.getDeclaredMethod("addStudentsInfo", Student.class);
      addStudentInfo.setAccessible(true);
      addStudentInfo.invoke(group, new Student("Roman", 43));
      assertEquals(4, group.getStudentsList().size());
      addStudentInfo.invoke(group, new Student("Pavlo", 35));
      assertEquals(5, group.getStudentsList().size());
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  private void assertArrayEquals(ArrayList<Student> sortedList,
      ArrayList<Student> startStudentList) {
    if (sortedList.size() == startStudentList.size()) {
      for (int i = 0; i < sortedList.size(); i++) {
        if (sortedList.get(i).getScore() == startStudentList.get(i).getScore()) {
          continue;
        }
        fail("Bad sorting");
      }
    } else {
      fail("Lists have different size");
    }
  }

  @AfterClass
  public static void clean() {
    group = null;
  }
}

