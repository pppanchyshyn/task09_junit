package com.epam.task09.junit;

public class Student {

  private String name;
  private double score;

  Student() {
  }

  Student(String name, double score) {
    this.name = name;
    this.score = checkInputScore(score);
  }

  public String getName() {
    return name;
  }

  public double getScore() {
    return score;
  }

  private double checkInputScore(double score) {
    if (score >= 0 && score <= 100) {
      return score;
    } else {
      return 0;
    }
  }
}
