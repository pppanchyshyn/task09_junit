package com.epam.task09.junit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;

public class Group {
    private static final Logger LOGGER = LogManager.getLogger(Group.class);
    private String groupName;
    private int studentsCount;
    private ArrayList<Student> studentsList;

    Group(String groupName, int studentsCount, ArrayList<Student> studentsList) {
        this.groupName = groupName;
        this.studentsCount = studentsCount;
        this.studentsList = studentsList;
    }

    public ArrayList<Student> getStudentsList() {
        return studentsList;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public String getGroupName() {
        return groupName;
    }

    public ArrayList<Student> sortStudents() {
        studentsList.sort(Comparator.comparingDouble(Student::getScore));
        return studentsList;
    }

    public void showStudentList() {
        if (!isStudentListEmpty() && isStudenListNotNull()) {
            for (int i = 0; i < studentsList.size(); i++) {
                LOGGER.info(studentsList.get(i).getName() + ": " + studentsList.get(i).getScore());
            }
        } else LOGGER.info("Student list is empty");
    }

    private void addStudentsInfo(Student student) {
        studentsList.add(student);
    }

    private boolean isStudentListEmpty() {
        return studentsList.isEmpty();
    }

    private boolean isStudenListNotNull() {
        return studentsList != null;
    }
}
